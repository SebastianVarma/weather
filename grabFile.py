import requests
from urllib.request import urlopen
from contextlib import closing
import gzip
import shutil
from ftplib import FTP
import subprocess
import pandas as pd
from datetime import date, timedelta

def getNumsGeneral(codeList, inDateStart, inDateEnd): #Format: YEAR-MONTH-DAY
	startDate = date(int(inDateStart.split("-")[0]), int(inDateStart.split("-")[1]), int(inDateStart.split("-")[2]))
	endDate = date(int(inDateEnd.split("-")[0]), int(inDateEnd.split("-")[1]), int(inDateEnd.split("-")[2]))
	listOfDataFrames = []
	for code in codeList:
		while startDate < endDate:
			inDict = getNums(code, str(startDate.year) + "-01-01")
			listOfDataFrames.append(grabFile(str(inDict["USAF"]), str(inDict["WBAN"]), str(startDate.year)))
			startDate = startDate + timedelta(days=365)
		inDict = getNums(code, str(endDate.year) + "-01-01")
		listOfDataFrames.append(grabFile(str(inDict["USAF"]), str(inDict["WBAN"]), str(endDate.year)))
	return pd.concat(listOfDataFrames)

def getNums(code, inDate): #Format: YEAR-MONTH-DAY
	splitDate = inDate.split("-")
	theDate = date(int(splitDate[0]), int(splitDate[1]), int(splitDate[2]))
	data = pd.read_csv("isd-history.csv")
	rows = data.loc[data["ICAO"] == code]
	for row in rows.iterrows():
		print(type(row[1]))
		startDateText = str(row[1]["BEGIN"])
		endDateText = str(row[1]["END"])
		startDate = date(int(startDateText[0:4]), int(startDateText[4:6]), int(startDateText[6:]))
		endDate = date(int(endDateText[0:4]), int(endDateText[4:6]), int(endDateText[6:]))
		if startDate <= theDate <= endDate:
			return {"USAF" : row[1]["USAF"], "WBAN" : row[1]["WBAN"]}

def grabFile(num1, num2, year):
	url = "ftp://ftp.ncdc.noaa.gov/pub/data/noaa/" + year + "/" + num1 + "-" + num2 + "-" + year + ".gz"
	ftp = FTP("ftp.ncdc.noaa.gov")
	ftp.login()
	ftp.cwd("pub/data/noaa/" + year)
	ftp.retrbinary("RETR " + url.split("/")[-1], open(url.split("/")[-1], "wb").write)
	with gzip.open(url.split("/")[-1], "rb") as f_in:
		with open(url.split("/")[-1][:-3] + ".txt", "wb") as f_out:
			shutil.copyfileobj(f_in, f_out)
	haz = subprocess.Popen("java -classpath . ishJava" + " " + url.split("/")[-1][:-3] + ".txt " + "output.txt", shell=True)
	data = pd.read_csv("output.txt", sep=" ", error_bad_lines=False)
	return data
